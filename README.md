# mPaani recruitment test

## Question

Build the backend system for a simple Tic-Tac-Toe game.

0. A user enters the system by providing a unique user name (no passwords/authentication).
0. He/she can then see all the players online.
0. He/she can challenge anyone for a game.
0. The game starts as soon as the other player accepts the challenge. Challenges can be rejected as well.
0. Once accepted, the players start playing one move at a time. A move basically consists of marking a cell (x, y) either as 'X' or 'O'.
0. Once the end state is reached, the winner gets registered and the game terminates. 
0. A player can undo his current move as long as the next player has not made his move.
0. For a particular player, list all historic games with stats like games played, games won, games lost, games drawn, average moves, fastest win etc.
0. List all the moves of a particular game.

## Solution

This is the Django solution.

## How-to ...?

<!-- TODO -->