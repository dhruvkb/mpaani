from rest_framework import views, response


class HelloWorld(views.APIView):
    """
    Hello World!
    """

    def get(self, request, *args, **kwargs):
        """
        Handle GET request
        :param request: the request to handle
        :return: the response to the request
        """

        response_data = {
            'message_text': 'Hello World'
        }
        return response.Response(data=response_data)
