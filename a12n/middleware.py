from a12n.models import Player


class PlayerAuthMiddleware:
    """
    Populates the player in the request if the request is authenticated
    """

    def __init__(self, get_response):
        """
        Set the middleware up
        :param get_response: built-in to enable middleware chaining
        """

        self.get_response = get_response

    def __call__(self, request):
        """
        Perform that actual task of the middleware
        :param request: the request before being acted on
        :return: the response after being acted on
        """

        request.player = None
        if 'Authorization' in request.headers:
            try:
                auth_type, token = request.headers['Authorization'].split(' ')
                request.player = Player.objects.get(session_key=token)
            except Player.DoesNotExist:
                pass

        response = self.get_response(request)

        return response
