from rest_framework import permissions


class IsAuthenticated(permissions.BasePermission):
    """
    Allows access only to authenticated players
    """

    def has_permission(self, request, view):
        return bool(request.player)
