from django.urls import path

from a12n.views import Login, Logout

app_name = 'a12n'

urlpatterns = [
    path('login/', Login.as_view(), name='login'),
    path('logout/', Logout.as_view(), name='logout'),
]
