from rest_framework import response, generics

from a12n.models import Player
from a12n.permissions import IsAuthenticated
from a12n.serializers import PlayerSerializer


class Login(generics.CreateAPIView):
    """
    Log the given user in
    """

    def create(self, request, *args, **kwargs):
        """
        Handle create (POST) request
        :param request: the request to handle
        :return: the response for the request
        """

        username = request.data.get('username')

        player, _ = Player.objects.get_or_create(username=username)
        player.login()

        response_data = PlayerSerializer(instance=player).data
        return response.Response(data=response_data)


class Logout(generics.DestroyAPIView):
    """
    Log the given user in
    """

    permission_classes = [IsAuthenticated, ]

    def destroy(self, request, *args, **kwargs):
        """
        Handle destroy (DELETE) request
        :param request: the request to handle
        :return: the response for the request
        """

        player = request.player
        player.logout()

        response_data = PlayerSerializer(instance=player).data
        return response.Response(data=response_data)
