import uuid

from django.db import models


class Player(models.Model):
    """
    The main player model, the basis of all games and moves
    """

    username = models.CharField(
        max_length=63,
        primary_key=True,
    )
    session_key = models.UUIDField(
        null=True,
        blank=True,
    )

    def _update_session(self, new_session_key):
        """
        Set the player's session key to the given key, invalidating the old one
        :param new_session_key: the new session key for the player
        """

        self.session_key = new_session_key
        self.save(update_fields=['session_key', ])

    def login(self):
        """
        Assign a new session key to the player
        """

        self._update_session(uuid.uuid4())

    def logout(self):
        """
        Clear the current session key of the player
        """

        self._update_session(None)

    def __str__(self):
        """
        Return the string representation of the model
        :return: the string representation of the model
        """

        return self.username
