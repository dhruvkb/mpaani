from django.contrib import admin

from a12n.models import Player

admin.site.register(Player)
