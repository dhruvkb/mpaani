from django.db import models

from game.constants import CHALLENGE_STATUSES, INITIATED, ACCEPTED


class Challenge(models.Model):
    """
    An invitation to another player to have a match
    """

    sender = models.ForeignKey(
        to='a12n.Player',
        related_name='sent_challenges',
        on_delete=models.CASCADE,
    )
    recipient = models.ForeignKey(
        to='a12n.Player',
        related_name='received_challenges',
        on_delete=models.CASCADE,
    )

    status = models.CharField(
        max_length=3,
        choices=CHALLENGE_STATUSES,
        default=INITIATED,
    )

    @property
    def arrow(self):
        if self.status == INITIATED:
            return '⇒'
        elif self.status == ACCEPTED:
            return '⇔'
        else:  # self.status == REJECTED
            return '⇏'

    def __str__(self):
        """
        Return the string representation of the model
        :return: the string representation of the model
        """

        return f'{self.id}: {self.sender} {self.arrow} {self.recipient}'
