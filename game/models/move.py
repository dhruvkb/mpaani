import datetime

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models

from game.constants import SYMBOL_X, SYMBOL_O
from game.utils import board_array_to_string, board_array_to_game_state


class Move(models.Model):
    """
    The act of marking a grid square with a symbol during a game
    """

    game = models.ForeignKey(
        to='Game',
        related_name='moves',
        on_delete=models.CASCADE,
    )

    position_x = models.IntegerField(
        verbose_name='Position (x, _)',
        help_text='Integer in [0, 2]',
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2),
        ],
    )
    position_y = models.IntegerField(
        verbose_name='Position (_, y)',
        help_text='Integer in [0, 2]',
        validators=[
            MinValueValidator(0),
            MaxValueValidator(2),
        ],
    )

    player = models.ForeignKey(
        to='a12n.Player',
        on_delete=models.CASCADE,
    )

    class Meta:
        unique_together = ('game', 'position_x', 'position_y',)

    def move(self):
        """
        Update the state of the game after the move has been made
        """

        game = self.game

        board_array = game.board_array
        symbol = game.symbol_of(self.player)

        board_array[self.position_x][self.position_y] = symbol
        board_array[self.position_x][3][symbol] += 1
        board_array[3][self.position_y][symbol] += 1

        game.board_string = board_array_to_string(board_array)

        if symbol == SYMBOL_X:
            game.turn_holder = game.player_o
        else:  # symbol == SYMBOL_O
            game.turn_holder = game.player_x

        is_finished, winner = board_array_to_game_state(
            board_array=board_array,
            to_check=[symbol, ]
        )
        if is_finished:
            game.end_datetime = datetime.datetime.now()
        if winner == SYMBOL_X:
            game.winner = game.player_x
        elif winner == SYMBOL_O:
            game.winner = game.player_o
        else:
            game.winner = None

        game.save(update_fields=[
            'board_string',
            'turn_holder',
            'end_datetime',
            'winner',
        ])

    def unmove(self):
        """
        Update the state of the game after the move has been made
        """

        game = self.game

        board_array = game.board_array
        symbol = game.symbol_of(self.player)

        board_array[self.position_x][self.position_y] = None
        board_array[self.position_x][3][symbol] -= 1
        board_array[3][self.position_y][symbol] -= 1

        game.board_string = board_array_to_string(board_array)

        if symbol == SYMBOL_X:
            game.turn_holder = game.player_x
        else:  # symbol == SYMBOL_O
            game.turn_holder = game.player_o

        is_finished, winner = board_array_to_game_state(
            board_array=board_array,
        )
        if not is_finished:
            game.end_datetime = None
        if winner == SYMBOL_X:
            game.winner = game.player_x
        elif winner == SYMBOL_O:
            game.winner = game.player_o
        else:
            game.winner = None

        game.save(update_fields=[
            'board_string',
            'turn_holder',
            'end_datetime',
            'winner',
        ])


    def __str__(self):
        """
        Return the string representation of the model
        :return: the string representation of the model
        """

        symbol = self.game.symbol_of(self.player)
        coordinates = f'({self.position_x}, {self.position_y})'

        return f'{self.id}: {symbol} at {coordinates} in {self.game_id}'
