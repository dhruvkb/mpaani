import datetime
import random

from django.db import models

from game.constants import SYMBOL_X, SYMBOL_O, BLANK_BOARD
from game.utils import board_array_to_string, board_string_to_array


class Game(models.Model):
    """
    The interaction of two players over a three by three grid
    """

    start_datetime = models.DateTimeField(
        auto_now_add=True,
    )
    end_datetime = models.DateTimeField(
        blank=True,
        null=True,
    )

    challenge = models.OneToOneField(
        to='Challenge',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    player_x = models.ForeignKey(
        verbose_name='Player X',
        help_text='Player whose symbol is X (goes first)',
        to='a12n.Player',
        related_name='games_as_x',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )
    player_o = models.ForeignKey(
        verbose_name='Player O',
        help_text='Player whose symbol is O',
        to='a12n.Player',
        related_name='games_as_y',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    board_string = models.TextField(
        blank=True
    )
    turn_holder = models.ForeignKey(
        to='a12n.Player',
        related_name='games_in_turn',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    winner = models.ForeignKey(
        to='a12n.Player',
        related_name='games_won',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    @property
    def board_array(self):
        """
        Convert board string to a 2D array
        :return: the 2D array
        """

        return board_string_to_array(self.board_string)

    @property
    def turn_holder_symbol(self):
        """
        Get the symbol whose turn it is to play next
        :return: the symbol whose turn it is
        """

        return self.symbol_of(self.turn_holder)

    @property
    def is_finished(self):
        """
        Return whether or not the game is finished
        :return: whether or not the game is finished
        """

        return self.end_datetime is not None

    def set_players(self, player_one, player_two):
        """
        Randomly set one player as X and the other as O
        :param player_one: one of the two players in the game
        :param player_two: the other of the two players in the game
        """

        if random.random() <= 0.5:
            # Assign X and O randomly to the players
            player_one, player_two = player_two, player_one

        self.player_x = player_one
        self.player_o = player_two
        self.turn_holder = player_one
        self.save(update_fields=['player_x', 'player_o', 'turn_holder', ])

    def symbol_of(self, player):
        """
        Return the symbol of the given player in the game
        :param player: one of the two players in the game
        :return: the symbol of the given player
        :raise: AssertionError, if the player is not in the game
        """

        if self.player_x == player:
            return SYMBOL_X
        elif self.player_o == player:
            return SYMBOL_O
        else:
            raise AssertionError(
                'Player is not part of the game'
            )

    def finish(self):
        """
        Finish the game
        """

        self.end_datetime = datetime.datetime.now()
        self.save(update_fields=['winner', 'end_datetime', ])

    def save(self, *args, **kwargs):
        """
        Populate board string and defer to base implementation
        """

        if not self.board_string:
            self.board_string = board_array_to_string(BLANK_BOARD)
        return super().save(*args, **kwargs)

    def __str__(self):
        """
        Return the string representation of the model
        :return: the string representation of the model
        """

        return f'{self.id}: (X = {self.player_x}) × (O = {self.player_o})'
