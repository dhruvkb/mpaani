from django.contrib import admin
from django.contrib.admin import register

from game.models import Challenge, Game, Move

admin.site.register(Challenge)


class MoveAdmin(admin.TabularInline):
    model = Move


@register(Game)
class GameAdmin(admin.ModelAdmin):
    model = Game
    inlines = [
        MoveAdmin
    ]
