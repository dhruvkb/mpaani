from rest_framework import serializers

from game.models import Game


class GameSerializer(serializers.ModelSerializer):
    is_my_turn = serializers.SerializerMethodField()
    board_array = serializers.SerializerMethodField()

    class Meta:
        model = Game
        fields = [
            'id',

            'start_datetime',
            'end_datetime',

            'challenge',

            'board_array',
            'is_my_turn',

            'is_finished',
            'winner',
        ]

    def get_board_array(self, instance):
        """
        Crop the performance bits from the array
        :param instance: the instance being serialized
        """

        return [instance.board_array[i][:3] for i in range(3)]

    def get_is_my_turn(self, instance):
        """
        Evaluate whether it is the turn of the client
        :param instance: the instance being serialized
        """

        return instance.turn_holder == self.context.get('request').player
