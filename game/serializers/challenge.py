from rest_framework import serializers

from game.constants import ACCEPTED, REJECTED
from game.models import Challenge


class ChallengeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Challenge
        fields = ['id', 'sender', 'recipient', 'status', 'game', ]
        read_only_fields = ['id', 'sender', 'game', 'status', ]

    def validate_recipient(self, recipient):
        if self.context.get('request').player == recipient:
            raise serializers.ValidationError(
                'You cannot challenge yourself'
            )

        return recipient


class ChallengeUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Challenge
        fields = ['id', 'sender', 'recipient', 'status', 'game', ]
        read_only_fields = ['id', 'sender', 'recipient', 'game', ]

    def validate_status(self, status):
        if status not in [ACCEPTED, REJECTED]:
            raise serializers.ValidationError(
                'Challenges can only be accepted or declined'
            )

        return status
