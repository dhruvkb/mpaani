from rest_framework import serializers

from game.models import Move


class MoveSerializer(serializers.ModelSerializer):
    symbol = serializers.SerializerMethodField()

    class Meta:
        model = Move
        fields = ['id', 'game', 'position_x', 'position_y', 'symbol', 'player', ]
        read_only_fields = ['id', 'player', 'symbol', ]

    def get_symbol(self, instance):
        """
        Get the symbol of the player whose turn it is to move
        :param instance: the instance being serialized
        """

        return instance.game.symbol_of(instance.player)

    def validate_game(self, game):
        if game.is_finished:
            raise serializers.ValidationError(
                'You cannot modify a finished game'
            )
        return game

    def validate(self, attrs):
        game = attrs.get('game')
        if game.turn_holder != self.context.get('request').player:
            raise serializers.ValidationError(
                'You cannot make a move out of your turn'
            )
        return attrs


class MoveUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Move
        fields = ['id', 'position_x', 'position_y', 'symbol', 'player', ]
        read_only_fields = ['id', 'symbol', 'player', ]

    def get_symbol(self, instance):
        """
        Get the symbol of the player whose turn it is to move
        :param instance: the instance being serialized
        """

        return instance.game.symbol_of(instance.player)

    def validate(self, attrs):
        game = self.instance.game
        if game.turn_holder == self.context.get('request').player:
            raise serializers.ValidationError(
                'You cannot edit your move after your opponent has moved'
            )
        return attrs
