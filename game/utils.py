import json

from game.constants import SYMBOL_X, SYMBOL_O


def board_string_to_array(board_string):
    """
    Convert board string to a 2D array
    :param board_string: the string representation of the 2D array
    :return: the 2D array
    """

    return json.loads(board_string)


def board_array_to_string(board_array):
    """
    Convert 2D board array to string
    :param board_array: the 2D array
    :return: the string representation of the 2D array
    """

    return json.dumps(board_array)


def board_array_to_game_state(board_array, to_check=None):
    """
    Determine state of the game from its 2D board array
    :param board_array: the 2D array
    :param to_check: the list of symbols to check for winning
    :return: the status of the game
    """

    is_finished = False
    winner = None
    if to_check is None:
        to_check = [SYMBOL_X, SYMBOL_O]

    for symbol in to_check:
        for i in range(0, 3):
            if board_array[3][i][symbol] == 3 or board_array[i][3][symbol] == 3:
                return True, symbol

        if all([
            board_array[0][0] == symbol,
            board_array[1][1] == symbol,
            board_array[2][2] == symbol,
        ]) or all([
            board_array[0][2] == symbol,
            board_array[1][1] == symbol,
            board_array[2][0] == symbol,
        ]):
            return True, symbol

    if all([board_array[i][j]
            for i in range(0, 3)
            for j in range(0, 3)]):
        return True, None

    return is_finished, winner
