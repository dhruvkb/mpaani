from django.urls import path, include
from rest_framework import routers

from game.views.challenge import ChallengeViewSet
from game.views.game import GameViewSet
from game.views.move import MoveViewSet
from game.views.stats import Stats

app_name = 'game'

router = routers.DefaultRouter()
router.register(r'challenges', ChallengeViewSet, basename='challenge')
router.register(r'games', GameViewSet, basename='game')
router.register(r'moves', MoveViewSet, basename='move')

urlpatterns = [
    path('stats/', Stats.as_view(), name='stats'),

    path('', include(router.urls)),
]
