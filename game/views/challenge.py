from django.db.models import Q
from rest_framework import viewsets

from a12n.permissions import IsAuthenticated
from game.constants import INITIATED, ACCEPTED
from game.models import Challenge, Game
from game.serializers.challenge import (
    ChallengeSerializer,
    ChallengeUpdateSerializer,
)


class ChallengeViewSet(viewsets.ModelViewSet):
    """
    Viewset for LRCUD operations on Challenge instances
    """

    permission_classes = [IsAuthenticated, ]

    def get_serializer_class(self):
        if self.request.method == 'PATCH':
            return ChallengeUpdateSerializer
        else:
            return ChallengeSerializer

    def get_queryset(self):
        if self.request.method == 'PATCH':
            return Challenge.objects.filter(
                Q(recipient=self.request.player) & Q(status=INITIATED)
            )
        elif self.request.method == 'DELETE':
            return Challenge.objects.filter(
                Q(sender=self.request.player) & Q(status=INITIATED)
            )
        else:
            return Challenge.objects.filter(
                Q(sender=self.request.player) | Q(recipient=self.request.player)
            )

    def perform_create(self, serializer):
        """
        Set the sender to the current user and save the challenge
        :param serializer: the populated serializer, ready to save
        """

        serializer.save(sender=self.request.player)

    def perform_update(self, serializer):
        """
        Update the challenge and initiate games for accepted challenges
        :param serializer: the populated serializer, ready to save
        """

        challenge = serializer.save()
        if challenge.status == ACCEPTED:
            game = Game.objects.create(challenge=challenge)
            game.set_players(challenge.sender, challenge.recipient)
