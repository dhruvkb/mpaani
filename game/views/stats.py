from django.db.models import Q
from rest_framework import generics, response

from a12n.permissions import IsAuthenticated
from game.models import Game


class Stats(generics.RetrieveAPIView):
    """
    Get the statistics pertaining to the current user
    """

    permission_classes = [IsAuthenticated, ]

    def retrieve(self, request, *args, **kwargs):
        """
        Handle retrieve (GET) request
        :param request: the request to handle
        :return: the response for the request
        """

        player = request.player
        games_played = Game.objects.filter(
            Q(player_x=player) | Q(player_o=player)
        )

        # Games that have the winner set to player
        games_won = player.games_won.all()
        # Games that never ended
        games_abandoned = games_played.filter(end_datetime=None)
        # Games taken to completion
        games_completed = games_played.exclude(end_datetime=None)
        # Games that ended without winners
        games_drawn = games_completed.filter(winner=None)
        # Games that ended with the other person winning
        games_lost = games_completed.exclude(winner=None).exclude(winner=player)

        response_data = {
            'counts': {
                'played': games_played.count(),
                'won': games_won.count(),
                'abandoned': games_abandoned.count(),
                'completed': games_completed.count(),
                'drawn': games_drawn.count(),
                'lost': games_lost.count(),
            }
        }
        return response.Response(data=response_data)
