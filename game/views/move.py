from django.db.models import Q
from rest_framework import viewsets, mixins

from a12n.permissions import IsAuthenticated
from game.models import Move
from game.serializers.move import (
    MoveSerializer,
    MoveUpdateSerializer,
)


class MoveViewSet(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    viewsets.GenericViewSet
):
    """
    Viewset for LCU operations on Move instances
    """

    permission_classes = [IsAuthenticated, ]

    def get_serializer_class(self):
        if self.request.method == 'PATCH':
            return MoveUpdateSerializer
        else:
            return MoveSerializer

    def get_queryset(self):
        if self.request.method == 'PATCH':
            queryset = Move.objects.filter(
                Q(player=self.request.player)
            )
        else:
            queryset = Move.objects.filter(
                Q(game__player_x=self.request.player)
                | Q(game__player_o=self.request.player)
            )

        game = self.request.query_params.get('game', None)
        if game is not None:
            queryset = queryset.filter(game=game)

        queryset = queryset.order_by('id')
        return queryset

    def perform_create(self, serializer):
        """
        Set the player to the current user, save the move and update the game
        :param serializer: the populated serializer, ready to save
        """

        move = serializer.save(player=self.request.player)
        move.move()

    def perform_update(self, serializer):
        """
        Update the move and update the game
        :param serializer: the populated serializer, ready to save
        """

        move = serializer.instance
        move.unmove()
        move = serializer.save()
        move.move()
