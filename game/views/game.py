from django.db.models import Q
from rest_framework import viewsets, mixins

from a12n.permissions import IsAuthenticated
from game.models import Game
from game.serializers.game import GameSerializer


class GameViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    viewsets.GenericViewSet
):
    """
    Viewset for LR operations on Game instances
    """

    permission_classes = [IsAuthenticated, ]
    serializer_class = GameSerializer

    def get_queryset(self):
        return Game.objects.filter(
            Q(player_x=self.request.player) | Q(player_o=self.request.player)
        )
