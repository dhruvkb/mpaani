SYMBOL_X = 'X'
SYMBOL_O = 'O'

SYMBOL_CHOICES = (
    (SYMBOL_X, SYMBOL_X),
    (SYMBOL_O, SYMBOL_O),
)

INITIATED = 'ini'
ACCEPTED = 'acc'
REJECTED = 'rej'

CHALLENGE_STATUSES = (
    (INITIATED, 'Initiated'),
    (ACCEPTED, 'Accepted'),
    (REJECTED, 'Rejected'),
)

BLANK_BOARD = [
    [None, None, None, {'X': 0, 'O': 0}],
    [None, None, None, {'X': 0, 'O': 0}],
    [None, None, None, {'X': 0, 'O': 0}],
    [{'X': 0, 'O': 0}, {'X': 0, 'O': 0}, {'X': 0, 'O': 0}, 'DKB']
]
